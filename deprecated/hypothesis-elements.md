# Elements of the Trustable hypothesis

The original [Trustable hypothesis](../hypothesis.md) argues that there are a number of different (but arguably equally valid) factors that may contribute to our trust in software, but these are specific to open source software and Continuous Integration / Delivery workflows. Existing approaches to trustability, such as compliance with an applicable standard, explicitly favour different approaches, but these:
* Are frequently followed in name only or applied retrospectively
* Are not relevant or practical in every context
* May not be effective in producing the desired outcomes
* Fail to address some of the trust-related issues that we care about

We believe that, by identifying a wider set of 'trust factors' and showing how they can combine, it is possible to construct a new methodology for evaluating trustability, which is more widely applicable to different domains and practices and based on evidence of the practices involved in the creation and refinement of the software. This evidence should allow us to both examine the merits of these practices and determine how consistently they have been used.

The following diagram illustrates all of the factors that we have identified, including those employed by existing formal approaches to trust, and describes their relationships with each other. It distinguishes between
* **Aspects** (blue): Abstract qualities of the software, which may inform a decision about its trustability
* **Factors** (yellow): Concrete properties of the software, or the practices involved in creating or refining it, which may provide supporting evidence for one or more *aspects*

![](hypothesis-diagram.svg)

## Aspects
These elements represent different aspects of trustability. The relative importance of these aspects may vary. Some aspects may relate to others.

* **Provenance**: We know where it comes from
  * Supported by: *Origin*, *Record*, *Credibility* and *Transparency*
* **Integrity**: We know what it is and how it is created and refined
  * Supported by *Conformance*, *Reproducibility*, *Transparency*, *Constructability* and *Testability*
* **Reliability**: We can be confident that it will continue to work as expected over time
  * Supported by *Record* and *Compliance*
  * Informed by *Functionality*
* **Durability**: We can update it and be confident that it will not break or regress
  * Supported by *Verifiability*, *Testability* and *Updatability*
  * Informed by *Functionality*
* **Functionality**: It does what it is supposed to do, and it does not do what it is not supposed to do
  * Supported by *Compliance*, *Safety*, *Security*, *Verifiability* and *Testability*

## Factors
These elements provide supporting evidence for one or more *Aspects*; in some cases, this evidence may relate to another *Factor*. For example, *Compliance* evidence supports *Functionality* or *Reliability*; some *Compliance* evidence may be related to *Safety* or *Security*.

* **Origin**: We can see who created or contributed to it
* **Credibility**: We can see opinions from others about it
  * May provide evidence related to *Origin* rather than the software itself
* **Record**: We can see its history or track record
  * May provide evidence related to *Origin* rather than the software itself
* **Scrutiny**: We can see opinions about its creation and refinement processes
  * Examples include peer review and audit against a formal standard
  * May be provided in lieu of *Transparency*
  * May support *Credibility*
* **Reproducibility**: We can reproduce it
  * Applies to both the construction and the deployment of the software
* **Transparency**: We can see how it is created and refined
  * May be provided in lieu of *Reproducibility*, *Constructability* or *Testability*
* **Constructability**: We know how to build it
  * Required for *Reproducibility*; may be needed for *Updatability*
* **Updatability**: We know how to update it
* **Compliance**: We can see attestations that it satisfies a defined set of criteria
  * May provide evidence of *Safety* or *Security*
* **Safety**: It is designed to protect against specific unintended losses
  * Feeds into *Verifiability*
* **Security**: It is designed to protect against unauthorised use or misuse
  * Feeds into *Verifiability*
* **Verifiability**: We know what it is supposed to do, and what it is not supposed to do
  * Required for *Testability*
* **Testability**: We know how to verify it before using it
