# Roles

The tasks required to develop trustable software can be attributed to a number
 of distinct Roles:

- Roles are performed by contributors (**t.contributor**).
- Each contributor (**t.contributor**) may be authorised to perform a different
 Role at different times.
- Multiple contributors (**t.contributor**) can perform the same Role at
 the same time.

## Customer

The consumer of the software product and the target for the delivery of the
 development effort.

Responsible for the definition of intents (**t.intent**).

## Owner

Responsible for the relationship with the Customer.

Responsible for the definition, collation and review of requirements (**t.requirement**).

Responsible for the review of features (**t.feature**).

Responsible for acceptance of the software (**t.software**) supported by the
 evidence (**t.evidence**).

## Lead

Responsible for the architecture, including definition of dependencies
 (external **t.software**) and other requirements
 (**t.requirement**) for the software (**t.software**) and the environments
 (**t.environment**).

Responsible for the definition of features (**t.feature**).

Responsible for the collation and delivery of estimates (**t.estimate**) for the
 software (**t.software**) and the environments (**t.environment**).

Responsible for the definition of the metrics (**t.metric**) to be generated.

## Developer

Responsible for implementation of features (**t.feature**) through submission of
 changes (**t.change**) which may include **t.sourcecode**, **t.build** and
 **t.test** for the software (**t.software**) and the environments
 (**t.environment**).

Responsible for production of estimates (**t.estimate**) by feature
 (**t.feature**) for the software (**t.software**) and the environments
 (**t.environment**).

## Reviewer

Responsible for reviewing the changes (**t.change**) which may include
 **t.sourcecode**, **t.build** and **t.test** for the software (**t.software**)
 and the environments (**t.environment**).

Responsible for reviewing that the tests (**t.test**) are exercising the
 behaviour of the features (**t.feature**).

## Gate

Responsible for responding to the submission of a change (**t.change**) by
triggering the Orchestrator.

Responsible for checking the result of the deployment of artefacts
(**t.artefact**) to a build environment (**t.environment**) and triggering the
Constructor.

Responsible for checking the result of the construction of artefacts
(**t.artefact**) and triggering the Orchestrator.

Responsible for checking the result of the deployment of artefacts
(**t.artefact**) to a validation environment (**t.environment**) and triggering
the Validator.

Responsible for checking the result of the validation of changes (**t.change**)
and triggering the Policy Handler.

## Orchestrator

Responsible for construction of an environment (**t.environment**) which produces
 evidence (**t.evidence**).

Responsible for deployment of artefacts (**t.artefact**) in an environment
 (**t.environment**) which produces evidence (**t.evidence**).

## Constructor

Responsible for executing a build (**t.build**) for the construction of artefacts
 (**t.artefact**) and production of evidence (**t.evidence**) in an environment
 (**t.environment**).

## Validator

Responsible for executing tests (**t.test**) which produce evidence
 (**t.evidence**) in an environment (**t.environment**).

Responsible for executing change validation tools (e.g. static code analysis tools,
 code coverage tools, etc.) which produce evidence (**t.evidence**).

## Policy Handler

Responsible for checking that submission, review, approval and merge of changes
(**t.change**) are performed in compliance with product policies.

Responsible for producing change review evidence (**t.evidence**).

## Change Tracker

Responsible for the aggregation and storage of changes (**t.change**) with a unique
 identifier.

Responsible for the ability to query such changes (**t.change**).

Responsible for the production of evidence (**t.evidence**) about the change (**t.change**).

## Evidence Tracker

Responsible for the aggregation and storage of all evidence (**t.evidence**)
generated.

Responsible for the ability to query such evidence (**t.evidence**).

## Artefact Tracker

Responsible for the aggregation and storage of artefacts (**t.artefact**) with a
 unique identifier.

Responsible for the ability to provide artefacts (**t.artefact**).

## Resource Tracker

Responsible for consumption of the evidence (**t.evidence**).

Responsible for generation of metrics (**t.metric**) about the production of the
 software (**t.software**).