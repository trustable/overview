# Change Submission Sequence Diagram

The sequence diagram shows the interactions between the [Roles](roles.md) during
the change submission scenario. This diagram only shows the scenario where all
operations are successful and the full sequence of events occur.

```mermaid
sequenceDiagram
  participant PCT as Change Tracker
  participant PG as Gate
  participant PO as Orchestrator
  participant PC as Constructor
  participant PV as Validator
  participant PPH as Policy Handler
  participant PAT as Artefact Tracker
  participant PET as Evidence Tracker

PCT->>+PG: Change submitted
loop Each build environment
  PG->>+PO: Construct build environment
  PO->>+PET: Store environment construction evidence
  PET-->>-PO:    
  PO-->>-PG: Environment construction result
  PG->>+PO: Deploy build environment artefacts
  PO->>+PAT: Retrieve artefacts
  PAT-->>-PO: Artefacts
  PO->>+PET: Store artefact deployment evidence
  PET-->>-PO:    
  PO-->>-PG: Artefact deployment result
  PG->>+PC: Construct artefacts
  PC->>+PAT: Store artefacts
  PAT-->>-PC:    
  PC->>+PET: Store artefact construction evidence
  PET-->>-PC:    
  PC-->>-PG: Artefact construction result
end
loop Each validation environment
  PG->>+PO: Construct validation environment
  PO->>+PET: Store environment construction evidence
  PET-->>-PO:    
  PO-->>-PG: Environment construction result
  PG->>+PO: Deploy validation environment artefacts
  PO->>+PAT: Retrieve artefacts
  PAT-->>-PO: Artefacts
  PO->>+PET: Store artefact deployment evidence
  PET-->>-PO:    
  PO-->>-PG: Artefact deployment result
  PG->>+PV: Validate change
  PV->>+PET: Store change validation evidence
  PET-->>-PV:    
  PV-->-PG: Change validation result
end
PG->>+PPH: Handle product policy
PPH->>+PET: Store change review evidence
PET-->>-PPH:    
PPH-->>-PG: Policy handling result
deactivate PG
```
