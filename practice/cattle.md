# Generic Requirements for self-contained online services

## Requirements

This is a list of things that are commonly required in online services
which are expected to be consumed by users and/or organisations.

- 24/7 operation, which implies
  - Service Level Agreement with uptime targets
  - measurement of performance against SLA
  - failure mode analysis and mitigations
  - known spinup times for all components and for whole service
  - redundancy for components where downtime would break the SLA
- self-serve for users of the service (e.g. they can signup, reset password)
- self-serve for operators of the service (e.g. they can apply upgrades, adjust configuration)
- self-serve for developers of the service (e.g. they can spinup a sandboxed version)
- service costs are measured and controllable
- compliance with applicable laws and regulations
- user security and privacy if applicable, which implies
  - identity
  - role-based and/or activity-based access control (RBAC, ABAC)

## Cattle vs Pets approach

"Cattle vs pets" is an established design pattern which aims to
satisfy the requirements above:

- services are delivered by a redundant set of components (cattle)
- no single point of failure - each component is disposable
  - destroying any component does not impact the service
  - each component's contribution is monitored (by other components)
  - when a component fails it is destroyed and a new one is instantiated
  - components do not hold special/unique state
  - the above is known to be atisfiable using 'ephemeral', 'immutable' designs
- instantiation and operation of services is via automation
- upgrades and rollbacks are via automation
- automation is via software
- software automation is triggered directly from SCM/VCS ("infrastructure as code")
- secrets/passwords are secured and separate from implementation instructions
