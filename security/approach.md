# WIP trustable:security approach

FIXME: aim towards worked example based on minimal distro
- STPA security approach (consider this after safety analysis)
- continuous vulnerability discovery policy
- CVE analysis
- CVE management cycle/process
- what other potential avenues are there?
- Which EAL would be relevant (Common Criteria) >> must depend on
the actual, specific case
- Which process & organisational compliance would fit best? (ISO 27001 & related)
