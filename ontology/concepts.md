# Concepts

## Introduction

This document defines set of terms, and their defined relationships and essential features, which can be used to model the practice of software development and its consumption by systems within a known context. Real-world processes and practices can be mapped onto these to facilitate analysis and comparison

It is based on a separately defined set of [elements and actions](elements.md), which can apply to the practice of creating and refining any body of work.

The following definitions apply throughout this document:
* **Elements** are the terms described in [elements.md](elements.md)
* **Concepts** are the terms defined in this document, which extend the Elements
* **Actions** are operations that impact specific Elements, as described in [elements.md](elements.md)
* **Characteristics** are the attributes of a given class of Elements or Concepts
* **Properties** are the attributes of a specific Element

The set of Concepts and their defining Characteristics are define din this document. Defining common Properties for these Concepts will be the objective of future work.

The keywords MAY, SHOULD and MUST in this document are to be interpreted as described in [RFC2119](https://www.ietf.org/rfc/rfc2119.txt).

## Overview

![Trustable concepts](trustable-concepts.png)

The set of Concepts is as follows:
* **t.system**: an entity constructed and instantiated to fulfil a defined set of t.intents in one or more t.contexts, which may entail executing t.software
* **t.software**: an aggregation of t.artifacts extracted from t.proposals from one or more t.histories
* **t.context**: describes a set of t.limits within which a t.system executes
* **t.limit**: a restriction on the permissible or possible behaviours within a t.context
* **t.intent**: an expression of goals, requirements or desires relating to a t.software or a t.system
* **t.constraint**: a verifiable aspect of the behaviour of a t.system.
* **t.process**: describes the order in which a series of *Actions* may occur
* **t.practice**: a representation of past *Actions* shown by evidence
* **t.policy**: an instance of recorded set of rules relating to a t.practice

*t.software* is an abstract representation of code and/or data, which is only realised when instantiated into some *t.system*, which may be entire or be composed of arbitrarily nested sub-*t.systems*. Everything beyond the highest level *t.system* under consideration is considered *t.context*, which may impose some external *t.limits* on the total capabilities of the *t.system*, where these might include things like the laws of physics, but might also include other factors beyond the control of the producing supply chain (e.g. affiliates, suppliers, customers, etc).

The precise separation of *t.system* and *t.context* is based on the organisation's *t.intents*: how the organisation decrees that the *t.system* should behave. As *t.intents* are usually written somewhat vaguely, analysis is performed to develop a set of *t.constraints* from the *t.intents*, where each *t.constraint* is explicitly verifiable. The total set of *t.limits* and *t.constraints* describe the scope of *t.system* behaviour within the *t.context*. Note that there is no expectation that *t.software* was developed with the same *t.intents* as those used for construction of a *t.system* consuming that *t.software*.

The creation of a *t.system* is described by some *t.process*, consisting of both descriptions of data flows and the *t.policy* that should govern specific Actions; for example, how to *Evaluate* a *t.proposal* for each specific *t.history* in the *t.process*. The actual creation of a *t.system* is done by some *t.practice*, where conformance to *t.process* or compliance to *t.policy* may be analysed by consideration of evidence in each *t.history* covered by the *t.process* or *t.policy*.

The defined *t.process* for considering a *t.proposal* for inclusion in a *t.history* may necessitate the creation of a 'candidate' *t.history* and/or *t.software* that is *Consumed* by an entity as part of that process; for example, a *t.software* derived from the *t.proposal* under consideration may need to be instantiated on a *t.system* in order to run a series of tests. The results of such actions may be made available as input to the *Evaluate* action via the *Record* action, in the form of *t.responses*.

## t.system
A t.system is an entity constructed and instantiated to fulfil a defined set of *t.intents* in one or more *t.contexts*, which may entail executing *t.software*. A *t.system* may itself be composed of subcomponents, each of which is a *t.system*.

Its behaviour is specified by *t.intents* and their associated *t.constraints*, as assessed against the *t.limits* of a known *t.context*.

**Characteristics**:
* A t.system MUST have one or more *t.intents*
* A t.system MAY have dependencies on other t.systems

## t.software
A t.software is an aggregation of [t.artifacts](elements.md#tartifact) extracted from [t.proposals](elements.md#tproposal) from one or more [t.histories](elements.md#thistory), which is only realised when instantiated into some *t.system*. That *t.system* may use it in order to realise its *t.intents* within a defined *t.context*.

A *Consume* action may require the instantiation of a *t.software*, which might draw its constituent *t.artifacts* from a number of discrete *t.histories* e.g. there may be one repository that contains code and another that contains (platform-specific) configuration files.

The *t.system* that instantiates a *t.software* may not be the 'target' for which that software was created; for example, one *t.system* may re-use a *t.software* component that was developed for another, or a test *t.system* may instantiate the *t.software* solely for the purposes of verifying its expected behaviour.

**Characteristics**:
* A t.software MAY be associated with more than one *t.system*
* A t.software MAY be composed of *t.artifacts* from a number of *t.histories*
* The specific aggregation of *t.proposals* from participating *t.histories* that are used by a t.software MAY be recorded by [*t.markers*](elements.md#tmarker).

## t.context
A t.context describes the *t.limits* within which a *t.system* executes. It is unbounded, but can only be understood in terms of its defined characteristics.

**Characteristics**:
* A t.context MAY impose *t.limits* on a *t.system*

## t.limit
A t.limit is a restriction on the permissible or possible behaviours within a *t.context*.

**Characteristics**:
* *t.intents* or *t.constraints* MAY be associated with *t.limits*

## t.intent
A t.intent is an expression of goals, requirements or desires relating to a *t.software* or a *t.system*.

**Characteristics**:
* t.intents SHOULD be expressed using the terminology used by the applicable business or other stakeholders
* t.intents SHOULD provide the base justification for all resulting *t.constraints*.
* t.intents MUST be represented in [t.proposals](elements.md#tproposal) as one or more [t.artifacts](elements.md#tartifact)
* t.intent SHOULD succinctly describe what constitutes success, including the purpose and conditions for the desired end state
* t.intent SHOULD relate the desired behaviour and the subordinate tasks to achieve this

## t.constraint
A t.constraint is a verifiable aspect of the behaviour of a *t.system*.

**Characteristics**:
* *t.constraints* MUST be represented in [t.proposals](elements.md#tproposal) as one or more [t.artifacts](elements.md#tartifact)
* All *t.constraints* MUST be verifiable
* All *t.constraints* SHOULD be machine readable
* Each *t.constraint* SHOULD be associated with one or more *t.intents*

## t.process
A t.process describes the order in which a series of *Actions* MAY occur. It may also provide specific instructions regarding the expected steps associated with those *Actions*.

**Characteristics**:
* A t.process MAY be congruent with a *t.policy*

## t.practice
A t.practice is a representation of past actions shown by evidence.

Evidence in this context consists of the *Properties* of the *Elements* involved in a t.practice, providing information about the *Actions* that impacted them.

**Characteristics**:
* A t.practice MAY be compliant with a *t.policy*
* A t.practice MAY be conformant with a *t.process*

## t.policy
A t.policy is an instance of recorded set of rules relating to a *t.practice*.

**Characteristics**:
* A t.policy MAY regulate a *t.practice*
