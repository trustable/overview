# Propositions

This document restates the Trustable Hypothesis as a set of formal propositions, in terms of the ontology's [concepts](concepts.md) and [elements](elements.md).

These propositions define the minimum set of criteria that is required for a second level of evaluation, which will calculate the level of confidence in an assessment based on statistical analysis of the available evidence. The integrity and authenticity of the evidence upon which these propositions are based may also need to be verified as part of this evaluation.

## Provenance
*“We know where it comes from”*

In order to have confidence in the Provenance of a given t.software, we must have:
* For every t.artifact associated with that t.software:
  * a t.identity that represents the attributed originator of that t.artifact
  * AND a t.proposal that asserts the identity of the attributed originator of that t.artifact
* AND For every t.proposal asserting the attributed originator of these artifacts:
  * a t.identity that represents the submitter of that t.proposal

***Proposition***

Given:
* *S* is “the t.software under consideration”

Let:
* *tP(S)* be “the Provenance of *S* is Trustable”
* *a* be “a specific version of a t.artifact”
* *S(a)* be “the complete set of *a* associated with *S*”
* *Ia* be “a t.identity that represents the attributed originator of *a*”
* *Pa* be “the t.proposal associated with *a*, which asserts *Ia*”
* *Ipa* be “a t.identity that represents the submitter of *Pa*”

Then:
#### *tP(S)* → ∀ *S(a)* (∃ *Ia* ∧ ∃ *Pa* ∧ (∀ *Pa* ( ∃ *Ipa*)))

## Construction
*“We know how to build it”*
*“We can reproduce it”*

In order to have confidence in the Construction of a given t.software:
* We must have :
  * t.artifacts that describe how that t.software is constructed
  * AND the t.artifacts that are required to construct that t.software
* AND repeatedly following the described instructions using the same inputs must always result in an identical set of constructed artifacts

***Proposition***
Given:
* *S* is “the t.software under consideration”

Let:
* *tC(S)* be “the Construction of *S* is Trustable”
* *a* be “a specific version of a t.artifact”
* *S(a)* be “the complete set of *a* associated with *S*”
* *Sx(a)* be “the subset of *S(a)* that is constructed”
* *Sxd(a)* be “the subset of *S(a)* that describes how to construct *Sx(a)*”
* *Sxc(a)* be “the subset of *S(a)* that is required to construct *Sx(a)* as described by *Sxd(a)*”
* *Sx(a)⁰* be “a version of *Sx(a)* constructed using *Sxc(a)* as described by *Sxd(a)*”
* *Sx(a)¹* be “a version of *Sx(a)* constructed independently from *Sx(a)⁰* using *Sxc(a)* as described by *Sxd(a)*”

Then:
#### *tC(S)* → ∃ *Sxd(a)* ∧ ∃ *Sxc(a)* ∧ (*Sx(a)⁰* = *Sx(a)¹*)

## Function
*“We know what it does”*
*“It does what it’s supposed to do”*

In order to have confidence in the Function of a given t.software for a given t.system, we must have:
* t.artifacts that describe:
  * one or more t.intents associated with that t.system
  * zero or more t.constraints associated with that t.system
* AND t.responses asserting that:
  * the t.software fulfils all of the described t.intents
  * the t.software satisfies all of the described t.constraints, where those t.constraints are associated with a described t.intent

***Proposition***

Given:
* *S* is "the t.software under consideration"
* *X* is "the t.system under consideration"

Let:
* *tF(S,X)* be "the Function of *S* for *X* is Trustable"
* *a* be "a specific version of a t.artifact"
* *S(a)* be "the set of *a* associated with *S*"
* *Si(a)* be "the subset of *S(a)* that represent t.intents relating to *X*"
* *Sc(a)* be "the subset of *S(a)* that represent t.constraints relating to *X*"
* *i* be "a specific t.intent"
* *S(i)* be "the set of *i* represented by *Si(a)*"
* *c* be "a specific t.constraint"
* *S(c)* be "the set of *c* represented by *Sc(a)*"
* *Si(c)* be "the subset of S(c) that are associated with a t.intent in *S(i)*"
* *o* be "a specific t.response related to *S*"
* *Si(o)* be "the set of *o* asserting that *i* is fulfilled by *S*"
* *Sc(o)* be "the set of *o* asserting that *c* is satisfied by *S*"

Then:
#### *tF(S,X)* → ∃ *Si(a)* ∧ *S(i)* ≠ ∅ ∧ ∃ *Sc(a)* ∧ ∀ *S(i)* (∃ *Si(o)*) ∧ ∀ *Si(c)* (∃ *Sc(o)*)

## Maintenance
*“We can update it and have confidence that it will not break or regress”*

TBD

## Protection
*"We have some confidence that it won't harm our communities and our children"*

The proposition (minimum criteria) for Protection is identical to Function, although additional criteria may be required at the second level of evaluation (level of confidence).
